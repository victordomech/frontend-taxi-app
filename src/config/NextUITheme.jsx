import { createTheme } from "@nextui-org/react";

const NextUITheme = createTheme({
	type: "dark", // light / dark
	className: "", // optional
	theme: {
		colors: {
			primary: "#FFC300",
			primaryDark: "#FFC300",
			secondary: "#003566",
			secondaryDark: "#003566",
			darkBlue: "#001D3D",
			lightYellow: "#FFD60A",
			gradient: "linear-gradient(112deg, $lightYellow 0%, $primaryDark 10%, $secondaryDark 80%)",
		},
		space: {},
		fontSizes: {},
		fonts: {},
		fontWeights: {},
		lineHeights: {},
		letterSpacings: {},
		sizes: {},
		borderWidths: {},
		borderStyles: {},
		radii: {},
		shadows: {},
		zIndices: {},
		transitions: {},
	},
});

export default NextUITheme;
