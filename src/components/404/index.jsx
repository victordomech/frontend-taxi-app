import { Container, Row, Text } from "@nextui-org/react";

const Page404 = () => {
  return (
    <Container style={{ height: "100vh" }} display="flex">
			<Row justify="center" align="center" style={{ flexDirection: "column" }}>
				<Text
					h1
					size={70}
					css={{
						textGradient: "45deg, $blue500 -20%, $pink500 50%",
						textAlign: "center",
					}}
					weight="bold"
				>
					404
				</Text>
				<Text
					h1
					size={40}
					css={{
						textGradient: "45deg, $yellow500 -20%, $red500 100%",
						textAlign: "center",
					}}
					weight="bold"
				>
                Parece que no hemos encontrado la página que buscas
                </Text>
				
			</Row>
		</Container>
  )
}

export default Page404