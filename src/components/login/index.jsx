import { Container, Row, Spacer, Text } from "@nextui-org/react";
import Login from "./form/login";
const LoginForm = () => {
	return (
		<Container style={{ height: "100vh" }} display="flex">
			<Row justify="center" align="center" style={{ flexDirection: "column" }}>
				<Text
					h1
					size={50}
					css={{
						textGradient: "45deg, $blue500 -20%, $pink500 50%",
						textAlign: "center",
					}}
					weight="bold"
				>
					Vamos a registrar
				</Text>
				<Text
					h1
					size={70}
					css={{
						textGradient: "45deg, $yellow500 -20%, $red500 100%",
						textAlign: "center",
					}}
					weight="bold"
				>
					una nueva carrera
				</Text>
				<Spacer y={4} />
				<Login />
			</Row>
		</Container>
	);
};

export default LoginForm;
