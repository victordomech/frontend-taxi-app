const initialState = {
	todoList: [],
};

const loginReducer = (state = initialState, action) => {
	const { type } = action;
	switch (type) {
		case "ADD_TODO": {
			return { ...state, todoList: [...state.todoList, action.todoText] };
		}

		default:
			return state;
	}
};

export default loginReducer;
