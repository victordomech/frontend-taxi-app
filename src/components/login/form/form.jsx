import { useFormikContext, Form as FormikForm } from "formik";
import { Input, Spacer, Col, Row } from "@nextui-org/react";
import SubmitButton from "./../../common/submitButton";
const Form = () => {
	const { values, setFieldValue, handleSubmit, isSubmitting } = useFormikContext();
	return (
		<Col style={{ maxWidth: "600px" }}>
			<FormikForm>
				<Input
					label="DNI / NIE"
					value={values.dni}
					onChange={val => setFieldValue("dni", val.target.value)}
					placeholder="12345678A"
					bordered
					required
					color={false}
					fullWidth
				/>
				<Spacer y={1} />

				<Input.Password
					label="Contraseña"
					value={values.password}
					onChange={val => setFieldValue("password", val.target.value)}
					placeholder="*************"
					color={false}
					bordered
					fullWidth
				/>
				<Spacer y={2} />

				<Row justify="end">
					<SubmitButton
						color="primary"
						type="submit"
						onClick={handleSubmit}
						loading={!!isSubmitting}
					>
						Iniciar sesión
					</SubmitButton>
				</Row>
			</FormikForm>
		</Col>
	);
};

export default Form;
