import { useContext } from "react";

import { Formik } from "formik";

import initialValues from "./initialValues";
import { Store } from "./../../../store/Store";
import Form from "./form";

const login = () => {
	const [state, dispatch] = useContext(Store);

	const onSubmit = (values, actions) => {
		console.log(values);
		setTimeout(() => {
			actions.setSubmitting(false);
		}, 5000);
	};

	return (
		<Formik
			initialValues={initialValues}
			onSubmit={onSubmit}
			validateOnChange={false}
			// enableReinitialize="true"
			validateOnBlur={false}
		>
			<Form />
		</Formik>
	);
};

export default login;
