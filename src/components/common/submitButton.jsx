import PropTypes from "prop-types";
import { Button, Loading } from "@nextui-org/react";

const SubmitButton = ({ loading, children, ...rest }) => {
	return (
		<Button {...rest} disabled={loading}>
			{loading ? <Loading color="white" size="sm" /> : children}
		</Button>
	);
};

SubmitButton.propTypes = {
	loading: PropTypes.bool.isRequired,
	children: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
};

export default SubmitButton;
