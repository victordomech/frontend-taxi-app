import React from 'react';
import MenuUnstyled from '@mui/base/MenuUnstyled';
import MenuItemUnstyled, {
  menuItemUnstyledClasses,
} from '@mui/base/MenuItemUnstyled';
import { buttonUnstyledClasses } from '@mui/base/ButtonUnstyled';
import PopperUnstyled from '@mui/base/PopperUnstyled';
import styled from 'styled-components';
import { Container, Row, Text } from "@nextui-org/react";

const blue = {
  100: '#DAECFF',
  200: '#99CCF3',
  400: '#3399FF',
  500: '#007FFF',
  600: '#0072E5',
  900: '#003A75',
};

const grey = {
  100: '#E7EBF0',
  200: '#E0E3E7',
  300: '#CDD2D7',
  400: '#B2BAC2',
  500: '#A0AAB4',
  600: '#6F7E8C',
  700: '#3E5060',
  800: '#2D3843',
  900: '#1A2027',
};

const StyledListbox = styled('ul')(
  ({ theme }) => `
  box-sizing: border-box;
  padding: 5px;
  margin: 10px 0;
  min-width: 200px;
  background: grey[900];
  border: 1px solid grey[800];
  border-radius: 0.75em;
  color: grey[300];
  overflow: auto;
  outline: 0px;
  `,
);

const StyledMenuItem = styled(MenuItemUnstyled)(
  ({ theme }) => `
  list-style: none;
  padding: 8px;
  border-radius: 0.45em;
  cursor: default;

  &:last-of-type {
    border-bottom: none;
  }

  &.${menuItemUnstyledClasses.focusVisible} {
    outline: 3px solid ${blue[600]};
    background-color: ${grey[800]};
    color: ${grey[300]};
  }

  &.${menuItemUnstyledClasses.disabled} {
    color: ${grey[700]};
  }

  &:hover:not(.${menuItemUnstyledClasses.disabled}) {
    background-color: ${grey[800]};
    color:  ${grey[300]};
  }
  `,
);

const TriggerButton = styled('button')(
  ({ theme }) => `
  min-height: calc(1.5em + 22px);
  min-width: 200px;
  background: ${grey[900]};
  border: 1px solid  ${grey[800]};
  border-radius: 0.75em;
  margin: 0.5em;
  padding: 10px;
  text-align: left;
  line-height: 1.5;
  color: ${grey[300]};

  &:hover {
    background: '' ;
    border-color:  ${grey[700]};
  }

  &.${buttonUnstyledClasses.focusVisible} {
    outline: 3px solid ${blue[600]};
  }

  &::after {
    content: '▾';
    float: right;
  }
  `,
);

const Popper = styled(PopperUnstyled)`
  z-index: 1;
`;

const Select = () => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const isOpen = Boolean(anchorEl);
  const buttonRef = React.useRef(null);
  const menuActions = React.useRef(null);

  const handleButtonClick = (event) => {
    if (isOpen) {
      setAnchorEl(null);
    } else {
      setAnchorEl(event.currentTarget);
    }
  };

  const handleButtonKeyDown = (event) => {
    if (event.key === 'ArrowDown' || event.key === 'ArrowUp') {
      event.preventDefault();
      setAnchorEl(event.currentTarget);
      if (event.key === 'ArrowUp') {
        menuActions.current?.highlightLastItem();
      }
    }
  };

  const close = () => {
    setAnchorEl(null);
    buttonRef.current.focus();
  };

  const createHandleMenuClick = (menuItem) => {
    return () => {
      console.log(`Clicked on ${menuItem}`);
      close();
    };
  };

  return (
    <div>
      <TriggerButton
        type="button"
        onClick={handleButtonClick}
        onKeyDown={handleButtonKeyDown}
        ref={buttonRef}
        aria-controls={isOpen ? 'simple-menu' : undefined}
        aria-expanded={isOpen || undefined}
        aria-haspopup="menu"
      >
        <Text style={{display: 'inline'}}>
            Language
        </Text>
      </TriggerButton>

      <MenuUnstyled
        actions={menuActions}
        open={isOpen}
        onClose={close}
        anchorEl={anchorEl}
        components={{ Root: Popper, Listbox: StyledListbox }}
        componentsProps={{ listbox: { id: 'simple-menu' } }}
      >
        <StyledMenuItem onClick={createHandleMenuClick('English')}>
          English
        </StyledMenuItem>
        <StyledMenuItem onClick={createHandleMenuClick('中文')}>中文</StyledMenuItem>
        <StyledMenuItem onClick={createHandleMenuClick('Português')}>
          Português
        </StyledMenuItem>
      </MenuUnstyled>
    </div>
  );
}

export default Select;