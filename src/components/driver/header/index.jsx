import { Container, Row, Text } from "@nextui-org/react";
import Select from "./../../common/select"

const Header = () => {
	return <header>
		<Container>
			<Row justify="space-between" align="center"	>
				<Text size={15} color="white" css={{ m: 0 }}>
					Menu
				</Text>
				<Select />
			</Row>
		</Container>

	</header>;
};

export default Header;
