import Header from "./header";
import Sheet from "./sheet"

const driverPage = () => {
  return (

    <nav>
        <div>driverPage</div>
        <Header />
        <Sheet />
    </nav>
  )
}

export default driverPage