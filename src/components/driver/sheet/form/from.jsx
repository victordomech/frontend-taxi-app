import { useFormikContext, Form as FormikForm } from "formik";
import { Input, Spacer, Col, Row } from "@nextui-org/react";
import SubmitButton from "./../../../common/submitButton";

const Form = () => {
	const { values, setFieldValue, isSubmitting } = useFormikContext();
	return (
		<Col style={{ maxWidth: "600px" }}>
			<FormikForm>
				<Input
					label="Servicios"
					value={values.services}
					onChange={val => setFieldValue("services", val.target.value)}
					type="number"
                    bordered
					required
					color={false}
					fullWidth
				/>
				<Spacer y={1} />

				<Input
					label="Km totales"
					value={values.totalKm}
					onChange={val => setFieldValue("totalKm", val.target.value)}
                    type="number"
                    labelRight="km"
					color={false}
					bordered
					fullWidth
				/>
				<Spacer y={1} />

                <Input
					label="Km ocupados"
					value={values.busyKm}
					onChange={val => setFieldValue("busyKm", val.target.value)}
                    type="number"
                    labelRight="km"
					color={false}
					bordered
					fullWidth
				/>
				<Spacer y={1} />

                <Input
					label="Kilómetros"
					value={values.carKm}
					onChange={val => setFieldValue("carKm", val.target.value)}
                    type="number"
                    labelRight="km"
					color={false}
					bordered
					fullWidth
				/>
				<Spacer y={1} />

                <Input
					label="Recaudado"
					value={values.earning}
					onChange={val => setFieldValue("earning", val.target.value)}
                    labelRight="€"
                    type="number"
					color={false}
					bordered
					fullWidth
				/>
				<Spacer y={1} />

				<Row justify="end">
					<SubmitButton
						color="primary"
						type="submit"
						loading={!!isSubmitting}
					>
						Iniciar carrera
					</SubmitButton>
				</Row>
			</FormikForm>
		</Col>
	);
};

export default Form;
