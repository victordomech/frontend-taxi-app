import Form from "./form/from"
import { Formik } from "formik";
import { Container, Row, Text } from "@nextui-org/react";
import initialValues from "./form/initialValues";


const sheet = () => {

  const onSubmit = (values, actions) => {
		console.log(values);
		setTimeout(() => {
			actions.setSubmitting(false);
		}, 5000);
	};

  return (
    <Container style={{ height: "100vh" }} display="flex">
      <Row justify="center" align="center" style={{ flexDirection: "column" }}>
      <Text
					h1
					size={50}
					css={{
						textGradient: "45deg, $yellow500 -20%, $red500 100%",
						textAlign: "center",
					}}
					weight="bold"
				>
					Completa tu carrera
				</Text>
        <Formik
          initialValues={initialValues}
          onSubmit={onSubmit}
          validateOnChange={false}
          // enableReinitialize="true"
          validateOnBlur={false}
        >
        <Form />
      </Formik>
      </Row>
    </Container>
  )
}

export default sheet