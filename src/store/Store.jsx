import React, { createContext, useReducer } from "react";
import PropTypes from "prop-types";
import { combineReducers } from "./CombineReducers";
import loginReducer from "../components/login/Reducer";

const initialState = {};

export const Store = createContext(initialState);

const appReducers = combineReducers({
	login: loginReducer,
});

export const StoreProvider = ({ children }) => {
	const [state, dispatch] = useReducer(appReducers, initialState);
	const store = React.useMemo(() => [state, dispatch], [state]);

	return <Store.Provider value={store}>{children}</Store.Provider>;
};

StoreProvider.propTypes = {
	children: PropTypes.element,
};
