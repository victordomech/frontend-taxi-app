export const combineReducers = reducers => (state, action) => {
	return Object.entries(reducers).reduce((newState, [reducerKey, reducer]) => {
		return {
			...newState,
			[reducerKey]: reducer(state[reducerKey], action),
		};
	}, state);
};
