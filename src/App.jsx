import { Route, Routes } from "react-router-dom";

import Home from "./components/home";
import Login from "./components/login";
import Driver from "./components/driver/page"
import Dashboard from "./components/admin/dashboard"
import Page404 from "./components/404"

function App() {
	return (
		<div className="App">
			<Routes>
				<Route path="/" element={<Home />} />
				<Route path="/login" element={<Login />} />
				<Route path="/driver" element={<Driver />} />
				<Route path="/dashboard" element={<Dashboard />} />
				<Route path="*" element={<Page404 />} />
			</Routes>
		</div>
	);
}

export default App;
