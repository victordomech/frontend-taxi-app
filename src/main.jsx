import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";

import { NextUIProvider } from "@nextui-org/react";

import { StoreProvider } from "./store/Store";
import App from "./App";
import Theme from "./config/NextUITheme";

ReactDOM.render(
	<StoreProvider>
		<NextUIProvider theme={Theme}>
			<BrowserRouter>
				<App />
			</BrowserRouter>
		</NextUIProvider>
	</StoreProvider>,
	document.getElementById("root")
);
